import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatService } from '../services/chat.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {
  livemsg = false;
  constructor(public router: Router, private ad: ActivatedRoute, private chat: ChatService) { }

  ngOnInit(): void {
  }
  ngAfterViewInit(){
    // this.countdownTimer();
    this.chat.getconnect('toujeo-165');
    this.chat.getSocketMessages('toujeo-165').subscribe((data => {
      // console.log('layout', data);
      if (data === 'start_live') {
        this.livemsg = true;
        setTimeout(() => {
          this.router.navigate(['eventhall']);
        }, 3000);
      }
      if (data === 'stop_live') {
        this.livemsg = false;
      }

    }));
  }
  // countdownTimer(){
  //   var countDownDate = new Date("Mar 15, 2021 16:56:25").getTime();

  //   // Update the count down every 1 second
  //   var x = setInterval(()=> {
  //     var now = new Date().getTime();
  //     var distance = countDownDate - now;
  //     if (distance < 0) {
  //       clearInterval(x);
  //       this.router.navigate(['eventhall']);
  //     }
  //   }, 1000);
  // }
}
