import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './header/header.component';
import { AgendaComponent } from './components/agenda/agenda.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { HelpDeskComponent } from './components/help-desk/help-desk.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpeakersComponent } from './components/speakers/speakers.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { SupportComponent } from './components/support/support.component';


@NgModule({
  declarations: [LayoutComponent, HeaderComponent, AgendaComponent, FeedbackComponent, HelpDeskComponent, SpeakersComponent, SupportComponent],
  imports: [PdfViewerModule,
    CommonModule,
    LayoutRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LayoutModule { }
