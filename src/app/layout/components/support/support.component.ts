import { Component, OnInit } from '@angular/core';
import { SteupServiceService } from 'src/app/services/steup-service.service';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {

  constructor(private _analytics: SteupServiceService,) { }

  ngOnInit(): void {
  }
  stepup(action) {
    this._analytics.stepUpAnalytics(action);
  }
}
