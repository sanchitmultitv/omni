import { Component, OnInit, HostListener } from '@angular/core';
import { fadeAnimation } from './shared/animation/fade.animation';
declare let ga: Function;

import { Router,RouterEvent, ActivationEnd,NavigationStart, ActivationStart } from '@angular/router';
import { FetchDataService } from './services/fetch-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent implements OnInit {
  title = 'virtualEvent';
  landscape = true;
  router: string;
  
  public getRouterOutletState(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }

  constructor(private _router: Router,private _fd: FetchDataService) {
    this.router = _router.url; 
  }

  ngOnInit(){
    this._fd.getLoginDetails().subscribe((res: any) => {
      if (res.code === 1) {
        localStorage.setItem('login_data', JSON.stringify(res.result));
      }else{
        window.location.reload()
      }
    });

    this._router.events.subscribe((event:RouterEvent)=> {
      if(event instanceof NavigationStart){
        //alert(event.url);
        ga('set', 'page', event.url);
        ga('send', 'pageview');
      }
     });

    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    }
  }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    } else{
      this.landscape = true;
    }
  }
}
