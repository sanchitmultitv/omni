import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {WebcamModule} from 'ngx-webcam';
import {SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { ThanksComponent } from './thanks/thanks.component';
import { GameComponent } from './@core/game/game.component';
import { BreakoutComponent } from './@core/breakout/breakout.component';
import { CometComponent } from './@core/comet/comet.component';
import { CometChatUI } from "../cometchat-pro-angular-ui-kit/CometChatWorkspace/projects/angular-chat-ui-kit/src/components/CometChatUI/CometChat-Ui/cometchat-ui.module";
import { HexagonComponent } from './@core/hexagon/hexagon.component';

import { ToastrModule } from 'ngx-toastr';
const data: SocketIoConfig ={ url : 'https://belive.multitvsolution.com:8030', options: {} };


@NgModule({
  declarations: [
    AppComponent,
    ThankyouComponent,
    ThanksComponent,
    GameComponent,
    BreakoutComponent,
    CometComponent,
    HexagonComponent,
  ],
  imports: [
    ReactiveFormsModule,
    CometChatUI,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule, 
    WebcamModule,
    SocketIoModule.forRoot(data),
    NgbModule,ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }