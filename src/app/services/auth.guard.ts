import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FetchDataService } from './fetch-data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {
  constructor(private authServ: FetchDataService, private router: Router){}
  // canActivate(): boolean {
  //   if(localStorage.getItem('virtual')){
  //   return true;
  //   }else{
  //     this.router.navigate(['/login'])
  //     return false
  //   }
  // }
  
}
