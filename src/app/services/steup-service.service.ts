import { Injectable } from '@angular/core';
import { FetchDataService } from './fetch-data.service';

@Injectable({
  providedIn: 'root'
})
export class SteupServiceService {

  constructor(private _fd: FetchDataService) { }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let hrs: any = new Date().getHours();
    let mns: any = new Date().getMinutes();
    let secs: any = new Date().getSeconds();
    // let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    if (hrs < 10) {
      hrs = '0' + hrs;
    }
    if (mns < 10) {
      mns = '0' + mns;
    }
    if (secs < 10) {
      secs = '0' + secs;
    }
    const formData = new FormData();
    formData.append('event_id', '165');
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', 'multitv');
    formData.append('designation', 'others');
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + hrs+':'+mns+':'+secs);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
}
