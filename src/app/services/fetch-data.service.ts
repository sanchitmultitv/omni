import { Injectable } from '@angular/core';
import { Login } from './module/login';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiConstants } from './apiConfig/api.constants';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  baseUrl = environment.baseUrl;
  loginBaseUrl = environment.url;
  login = 'RecognizerApi/index.php/api/engine/authentication';


  constructor(private http: HttpClient) { }
  authLogin(user: Login): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('email', user.email);
    params = params.set('password', user.password);
    return this.http.post(`${this.loginBaseUrl}/${this.login}`, params);
  }

  getProfile(id, role_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.profile}/id/${id}/role_id/${role_id}`);
  }
  getAttendees(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}`);
  }
  getAttendeesbyName(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}/name/${name}`);
  }
  getComments(event_id, user_id, typ): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.commentsList}/event_id/${event_id}/type/${typ}`);
  }
  postComments(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.addComment}`, comment);
  }
  getOne2oneChatList(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.chatAttendeesComments}/event_id/${event_id}/name/${name}`);
  }
  enterTochatList(receiver_id, sender_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.One2OneCommentList}/receiver_id/${receiver_id}/sender_id/${sender_id}`)
  }
  postOne2oneChat(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.one2oneCommentPost}`, comment);
  }
  getRating(): Observable<any> {
    return this.http.get(`https://goapinew.multitvsolution.com/virtualapi/v1/rating/master/list/event_id/165`);
  }
  getFeedback(): Observable<any> {
    return this.http.get(`https://goapinew.multitvsolution.com/virtualapi/v1/feedback/master/list/event_id/165`);
  }
  postFeedback(feedback: any): Observable<any> {
    return this.http.post(`https://goapinew.multitvsolution.com/virtualapi/v1/feedback/post`, feedback);
  }
  postRating(rating: any): Observable<any> {
    return this.http.post(`https://goapinew.multitvsolution.com/virtualapi/v1/rating/post`, rating);
  }
  getExhibition(title): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.exhibition}/${title}`);
  }
  getQuiz() {
    return this.http.get('https://opentdb.com/api.php?amount=50&category=11&type=multiple');
  }
  GetNetworkpage(event_id){
    return this.http.get(`${this.baseUrl}/virtualapi/v1/get/on/page/user/event_id/${event_id}`);
  }
  getLoginDetails(){
    return this.http.get(`https://goapi.multitvsolution.com:7002/builderapi/v1/eventBuilder/get/token/60a5030450af6`)
  }
  askQuestions(id,name, value,exhibit_id, exhibit_name): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('sender_id', id);
    params = params.set('sender_name', name);
    params = params.set('receiver_id', exhibit_id);
    params = params.set('receiver_name', exhibit_name);
    params = params.set('msg', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestion}`, params);
  }
  helpdesk(id,value,event_id): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('event_id', event_id);
    params = params.set('user_id', id);
    params = params.set('question', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.helpdesk}`, params);
  }
  gethelpdeskanswers(uid, e_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.helpdeskAnswes}/${uid}/event_id/${e_id}`)
  }
  askLiveQuestions(id, value, audi_id): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('user_id', id);
    params = params.set('question', value);
    params = params.set('event_id', '165');
    params = params.set('audi_id', audi_id);

    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestionLive}`, params);
  }
  getanswers(uid,exhibit_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getQuestionAnswser}/${uid}/sender_id/${exhibit_id}`)
  }
  // Liveanswers(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getliveAnswser}`)
  // }
  getPollList(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  }
  // getPollListTwo(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListThree(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListFour(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  postPoll(id, data, value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('poll_id', id);
    params = params.set('user_id', data);
    params = params.set('answer', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.postPolls}`, params);
  }
  getWtsappFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.wtsappAgendaFiles}`);
  }

  getQuizList(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.quizlist}/event_id/${event_id}`);
  }
  postSubmitQuiz(quiz): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.submitQuiz}`, quiz);
  }
  getSummaryQuiz(event_id, user_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.summaryQuiz}/event_id/${event_id}/user_id/${user_id}`);
  }

  uploadCameraPic(pic): Observable<any> {
    return this.http.post(`https://goapinew.multitvsolution.com/virtualapi/v1/upload/pic`, pic);
  }
  uploadsample(pic): Observable<any> {
    // return this.http.post(`https://tbbmedialive.com/Q3PCM2020/uploadblob.php`, pic);
    return this.http.post(`https://virtualapi.multitvsolution.com/upload_photo/uploadblob.php`, pic);
  }
  groupchating(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/audione`);
  }
  groupchatingStage(room:any): Observable<any>{
    return this.http.get(`https://d4rwaq6vwd4nr.cloudfront.net/chatapi/v1/get/mongo/groupchat/room/${room}`);
  }
  postGroupChatting(event_id, chat:any): Observable<any>{
    return this.http.post(`https://d4rwaq6vwd4nr.cloudfront.net/chatapi/v1/groupchat/post/event_id/${event_id}`, chat);
  }
  submitWheelScore(wheel: any) {
    return this.http.post(`https://goapinew.multitvsolution.com/virtualapi/v1/user/game/post`, wheel);
  }
  getBriefcaseList(event_id, user_id) {
    return this.http.get(`${this.baseUrl}/${ApiConstants.briefcaseList}/event_id/${event_id}/user_id/${user_id}`);
  }
  postBriefcase(doc: any) {
    return this.http.post(`${this.baseUrl}/${ApiConstants.postBriefcase}`, doc);
  }
  analyticsPost(analytics: any): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/analytics/user/history/add`, analytics)
  }
  activeAudi(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.activeInactive}`);
  }
  postPdsGroupChat(chat:any){
    return this.http.post(`${this.baseUrl}/${ApiConstants.pdsGroupChat}`, chat);
  }
  getPlayerUrl(event_id): Observable<any>{
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/auditorium/event_id/${event_id}`);
  }
}
