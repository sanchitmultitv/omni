import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { SteupServiceService } from 'src/app/services/steup-service.service';
declare var $: any;
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-hexagon',
  templateUrl: './hexagon.component.html',
  styleUrls: ['./hexagon.component.scss']
})
export class HexagonComponent implements OnInit {
  liveMsg = false;
  token = 'toujeo-165';
  getMsg
  sender_id: any;
  sender_name;
  searchChatList = [];
  receiver_id: any;
  chatMessage = [];
  textMessage = new FormControl('');
  receiver_name;
  allChatList = [];
  allChatIndex = 0;
  chatUser: any;

  constructor(private toastr: ToastrService, private chat: ChatService, private _analytics: SteupServiceService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.chat.getconnect(this.token);
    this.chat.getMessages().subscribe((data => {
      // console.log('HEILL'+data);
      let check = data.split('_');
      let datas = JSON.parse(localStorage.getItem('virtual'));
      this.chatUser = check[2];
      if (check[0] == 'one2one' && check[1] == datas.id) {
        this.toastr.success(this.chatUser + ' sent you a message');

        this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
          this.chatMessage = res.result;
        });
      }
        if (check[0] == 'start' && check[1] == 'live') {

          this.getMsg = check[2]
          this.liveMsg = true;
        }
        if (check[0] == 'stop' && check[1] == 'live') {

          this.liveMsg = false;
        }

        // console.log('testing', res);
      }));
    // this.getAllAttendees();
  }

  getAllAttendees() {

    let event_id = 165;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this._fd.getAttendees(event_id).subscribe((res: any) => {
      this.allChatList = res.result;
      this.searchChatList = res.result;
      this.receiver_id = res.result[0].id;
      this.receiver_name = res.result[0].name;
      this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
        this.chatMessage = res.result;

      });

      // this.timer = setInterval(() => {
      //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      //     this.chatMessage = res.result;
      //   });

      // }, 150000);
      this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
        this.chatMessage = res.result;
      });

    });
    $('.chatsModal').modal('show');

  }

  selectedChat(chat, ind) {
    // alert(chat)
    // alert(ind)
    this.allChatIndex = ind;
    let event_id = 165;
    this.sender_id = JSON.parse(localStorage.getItem('virtual')).id;
    this.sender_name = JSON.parse(localStorage.getItem('virtual')).name;
    this.receiver_id = chat.id;
    this.receiver_name = chat.name;
    console.log(chat)
    this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
      this.chatMessage = res.result;
    });
  }

  postOneToOneChat(event) {
    let msg = event.value;
    const formData = new FormData();
    formData.append('sender_id', this.sender_id);
    formData.append('sender_name', this.sender_name);
    formData.append('receiver_id', this.receiver_id);
    formData.append('receiver_name', this.receiver_name);
    formData.append('msg', msg);
    if (event.value !== null) {
      this._fd.postOne2oneChat(formData).subscribe(data => {
        this.textMessage.reset();
        this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
          this.chatMessage = res.result;

        });
      });
    }
    setTimeout(() => {
      $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight - 100;
    }, 1500);
    // this.timer = setInterval(() => {
    //   this._fd.enterTochatList(this.receiver_id, this.sender_id).subscribe(res => {
    //     this.chatMessage = res.result;
    //   });
    // }, 1000);
  }

  searchElement(query) {
    let event_id = 165;
    this._fd.getOne2oneChatList(event_id, query).subscribe(res => {
      this.allChatList = res.result;
    });
  }
  // allPerson(){
  //   let event_id = 165;
  //   this._fd.getAttendees(event_id).subscribe(res => {
  //     this.allChatList = res.result;
  //   });
  // }


  closePopup() {
    $('.chatsModal').modal('hide');
  }
  stepup(action) {
    this._analytics.stepUpAnalytics(action);
  }

}
