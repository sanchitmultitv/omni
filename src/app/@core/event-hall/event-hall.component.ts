import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
@Component({
  selector: 'app-event-hall',
  templateUrl: './event-hall.component.html',
  styleUrls: ['./event-hall.component.scss']
})
export class EventHallComponent implements OnInit {
  player:any;
  constructor(private router: Router, private _fd:FetchDataService) { }

  ngOnInit(): void {
    let event_id = 165;
    // let event_id = 165;
    this._fd.getPlayerUrl(event_id).subscribe((res:any)=>{
      let stream = res.result[0]['stream'];
      this.playVideo(stream);
    });
  }
  playVideo(stream) {
    var playerElement = document.getElementById("player-wrapper");
    this.player = new Clappr.Player({
      parentId: 'player-wrapper',
      source: stream,
      poster: 'assets/fasenra/poster.jpg',
      maxBufferLength: 30,
      height: '62.3%',
    width: '56.3%',
      // width: '115vh',
      // height: '30.2vw',
      autoPlay: true,
      loop: true,
      // width: window.innerWidth*.5781,
      // height: window.innerWidth*.5707/16*9,
      hideMediaControl: true,
      hideVolumeBar: true,
      hideSeekBar: true,
      persistConfig: false,
      // chromeless: true,
      // mute: true,
      visibilityEnableIcon: false,
      disableErrorScreen: true,
      playback: {
        playInline: true,
        // recycleVideo: Clappr.Browser.isMobile,
        recycleVideo: true
      },
    });
    this.player.attachTo(playerElement);    
  }
  gotoChat(){
    this.router.navigate(['/eventhall/chat']);
  }
  playAudioClap() {
    let playaudio: any = document.getElementById('myAudioClap');
    playaudio.play();
  }
  clapping(){
    let clap: any = document.getElementById('myAudioClap');
    clap.play();
  }
  whistling(){
    let whistle: any = document.getElementById('myAudioWhistle');
    whistle.play();
  }
  // @HostListener('window:resize', ['$event']) onResize(event) {
  //   this.player.resize({ width: window.innerWidth*.5781, height: window.innerWidth*.5707/16*9 });
  // }
}
