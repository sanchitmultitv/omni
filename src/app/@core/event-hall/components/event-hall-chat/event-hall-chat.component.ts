import { AfterViewInit, Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;
@Component({
  selector: 'app-event-hall-chat',
  templateUrl: './event-hall-chat.component.html',
  styleUrls: ['./event-hall-chat.component.scss']
})
export class EventHallChatComponent implements OnInit, AfterViewInit, OnDestroy {
  paramId;
  topic;
  topics=[
    'Equipment cleaning best practices',
    'Open forum',
    'Engineering and maintenance',
    'Virtual inspection',
    'Smoke studies',
    'Visual inspection for sod & parenteral',
  ];
  room_name;
  messageList = [];
  user_name;
  topicText = new FormControl('');
  constructor(private ad: ActivatedRoute, private router: Router, private _fd:FetchDataService, private chatService: ChatService) { }

  ngOnInit(): void {
    $('#eventHallChatModal').modal('show');
    this.user_name = JSON.parse(localStorage.getItem('virtual')).name;
    let event_id = JSON.parse(localStorage.getItem('virtual')).event_id;
    // let event_id = 138;
    // this.ad.params.subscribe((params: Params)=>{
    //   this.paramId = params.id;
    //   console.log('hey', this.paramId)
    //   this.room_name = `topic${this.paramId}`;
    //   this.getGroupChat(this.room_name);
    //   // this.room_name = 'room86';
    //   for (let i = 0; i < 6; i++) {      
    //     if(this.paramId == (i+1)){
    //       this.topic = this.topics[i];
    //     }  
    //   }
    // });
    this.room_name = `omni`;
    this.getGroupChat(this.room_name);
    this.chatService.getSocketMessages(`toujeo-${event_id}`).subscribe((data: any) => {
      if(data==='groupchat'){
        this.getGroupChat(this.room_name);
      }
    });
  }
  ngAfterViewInit(){

  }
  getGroupChat(room){
    this._fd.groupchatingStage(room).subscribe(res => {
      this.messageList = res.result;
    });
  }
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let chat: any = document.getElementById('eventHallChatModal');
    if(targetElement===chat){
      this.router.navigate(['/eventhall']);
    }
  }
  postTopicChat(value) {
    let yyyy: any = new Date().getFullYear();
    let mm: any = new Date().getMonth() + 1;
    let dd: any = new Date().getDate();
    let hour: any = new Date().getHours();
    let min: any = new Date().getMinutes();
    let sec: any = new Date().getSeconds();
    if (sec.toString().length !== 2) {
      sec = '0' + sec;
    } else {
      sec = sec;
    }
    if (min.toString().length !== 2) {
      min = '0' + min;
    } else {
      min = min;
    }
    if (hour.toString().length !== 2) {
      hour = '0' + hour;
    } else {
      hour = hour;
    }
    if (dd.toString().length !== 2) {
      dd = '0' + dd;
    } else {
      dd = dd;
    }
    if (mm.toString().length !== 2) {
      mm = '0' + mm;
    } else {
      mm = mm;
    }
    let created: any = `${yyyy}-${mm}-${dd} ${hour}:${min}:${sec}`;
    let data: any = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('event_id', data.event_id);
    formData.append('room_name', this.room_name);
    formData.append('user_name', data.name);
    formData.append('email', data.email);
    formData.append('chat_data', value);
    formData.append('is_approved', '1');
    formData.append('created', created);
    formData.append('company', data.company);
    if((value !== '')&&(value !== null)){
      this._fd.postGroupChatting(data.event_id, formData).subscribe(res => {
        console.log('posting', res);
      });
      this.topicText.reset();
    }
  }
  closeChat(){
    $('#eventHallChatModal').modal('hide');
    this.router.navigate(['/eventhall']);
  }
  ngOnDestroy(){
    this.room_name = '';
  }
}

