import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;
@Component({
  selector: 'app-hall-chat',
  templateUrl: './hall-chat.component.html',
  styleUrls: ['./hall-chat.component.scss']
})
export class HallChatComponent implements OnInit, AfterViewInit {
  textMessage = new FormControl('');
  messageList = [];
  user_name = JSON.parse(localStorage.getItem('virtual')).name;
  storage:any=JSON.parse(localStorage.getItem('virtual'));
  room_name = 'eventhall';
  constructor(private router: Router, private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    $('#chattingModal').modal('show');
  }
  ngAfterViewInit() {
    this.getGroupChat();
    let event_id =this.storage.event_id;
    // let event_id =138;
    this.chatService.getSocketMessages(`toujeo-${event_id}`).subscribe((data: any) => {
      console.log('testing', data)
      if(data==='groupchat'){
        this.getGroupChat();
      }
    });    
  }
  getGroupChat(){
    this._fd.groupchatingStage(this.room_name).subscribe(res => {
      this.messageList = res.result;
    });
  }
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let chat: any = document.getElementById('chattingModal');
    if(targetElement===chat){
      this.router.navigate(['/eventhall']);
    }
  }
  closeChat(){
    this.router.navigate(['/eventhall']);
  }
  
  postMessage(value) {
    let yyyy: any = new Date().getFullYear();
    let mm: any = new Date().getMonth() + 1;
    let dd: any = new Date().getDate();
    let hour: any = new Date().getHours();
    let min: any = new Date().getMinutes();
    let sec: any = new Date().getSeconds();
    if (sec.toString().length !== 2) {
      sec = '0' + sec;
    } else {
      sec = sec;
    }
    if (min.toString().length !== 2) {
      min = '0' + min;
    } else {
      min = min;
    }
    if (hour.toString().length !== 2) {
      hour = '0' + hour;
    } else {
      hour = hour;
    }
    if (dd.toString().length !== 2) {
      dd = '0' + dd;
    } else {
      dd = dd;
    }
    if (mm.toString().length !== 2) {
      mm = '0' + mm;
    } else {
      mm = mm;
    }
    let created: any = `${yyyy}-${mm}-${dd} ${hour}:${min}:${sec}`;
    let data: any = JSON.parse(localStorage.getItem('virtual'));
    const formData = new FormData();
    formData.append('event_id', data.event_id);
    formData.append('room_name', this.room_name);
    formData.append('user_name', data.name);
    formData.append('email', data.email);
    formData.append('chat_data', value);
    formData.append('is_approved', '1');
    formData.append('created', created);
    if((value !== '')&&(value !== null)){
      this._fd.postGroupChatting(data.event_id, formData).subscribe(res => {
        console.log('posting', res);
      });
      this.textMessage.reset();
    }
  }
}
