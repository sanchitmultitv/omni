import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventHallRoutingModule } from './event-hall-routing.module';
import { EventHallComponent } from './event-hall.component';
import { HallChatComponent } from './components/hall-chat/hall-chat.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QnaComponent } from './components/qna/qna.component';
import { EventHallChatComponent } from './components/event-hall-chat/event-hall-chat.component';


@NgModule({
  declarations: [
    EventHallComponent,
    HallChatComponent,
    QnaComponent,
    EventHallChatComponent],
  imports: [
    CommonModule,
    EventHallRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EventHallModule { }
